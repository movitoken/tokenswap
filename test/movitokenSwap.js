const MoviToken = artifacts.require("MoviToken");
const MoviToken2 = artifacts.require("MoviToken2");
const MoviTokenSwapManager = artifacts.require("MoviTokenSwapManager");

contract("MoviTokenSwapManager", async accounts => {
  let movi, movii, swap;

  before(async () => {
    movi = await MoviToken.deployed();
    swap = await MoviTokenSwapManager.deployed(movi);
    let moviiAddr = await swap.newMovi.call();
    movii = await MoviToken2.at(moviiAddr);
  });

  it("has entire movii balance", async () => {
    const swapAddr = await swap.address;
    const ownerBalance = await movii.balanceOf(swapAddr);
    assert.equal(ownerBalance.toString(), "1000000000000000", "owner has entire movi balance");
  });

  it("upgrades movi to movii", async () => {
    const approveTransfer = await movi.approve(swap.address, "1000000000", {from: accounts[0]});
    const allowance = await movi.allowance(accounts[0], swap.address);
    assert.equal(allowance.toString(), "1000000000", "allowance should be full supply");
    const withdrawTokens = await swap.withdrawTokens({from:accounts[0]});
    const moviBal = await movi.balanceOf(accounts[0]);
    const moviiBal = await movii.balanceOf(accounts[0]);
    const allowanceAfter = await movi.allowance(accounts[0], swap.address);
    assert.equal(allowanceAfter.toString(), "0", "allowance should be zero after transfer");
    assert.equal(withdrawTokens.logs[0].event, "TokensUpgraded", "TokensUpgraded event logged");
    assert.equal(moviBal.toString(), "0", "balance of movi after transfer should be zero");
    assert.equal(moviiBal.toString(), "1000000000", "balance of movii after transfer should be full supply");
  })
});
