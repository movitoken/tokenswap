pragma solidity ^0.5.0;

import 'openzeppelin-solidity/contracts/token/ERC20/ERC20.sol';
import 'openzeppelin-solidity/contracts/token/ERC20/ERC20Detailed.sol';
import 'openzeppelin-solidity/contracts/token/ERC20/ERC20Capped.sol';

contract MoviToken2 is ERC20, ERC20Detailed, ERC20Capped {
  uint256  private _totalSupply = 1000000000e6;
  constructor ()
    ERC20Detailed("MoviToken", "MOVI", 6)
    ERC20Capped(1000000000e6)
    ERC20()
    public
  {
    _mint(msg.sender, _totalSupply);
  }
}
