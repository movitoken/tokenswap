pragma solidity ^0.5.0;
import './MoviToken2.sol';
import './MoviToken1.sol';
import 'openzeppelin-solidity/contracts/token/ERC20/ERC20.sol';
import 'openzeppelin-solidity/contracts/math/SafeMath.sol';

contract MoviTokenSwapManager {
  using SafeMath for uint256;

  // the person who deployed this contract
  address public _manager;

  // The deployed mainnet MoviToken Contract
  //address _oldMoviAddress = 0x623b925b0a57a24ea8de301f2e3e692ce903f0c3;

  MoviToken internal oldMovi;
  MoviToken2 internal newMovi;

  event NewTokenDeployed(address newTokenContract);
  event TokensUpgraded(address tokenOwner, uint256 amount);

  modifier managerOnly {require(msg.sender == _manager); _;}

  modifier tokensDeployed() {
    require(address(oldMovi) != address(0));
    require(address(newMovi) != address(0));
    _;
  }

  constructor() public {
    oldMovi = MoviToken(address(0x623B925b0A57a24EA8dE301F2E3E692cE903f0c3));
    _manager = msg.sender;
    newMovi = new MoviToken2();
    emit NewTokenDeployed(address(newMovi));
  }

  function getNewMovi() view public returns (address) {
    return address(newMovi);
  }

  function withdrawTokens() public tokensDeployed returns (uint256){
    // ensure sender has tokens
    uint256 userBalance = oldMovi.balanceOf(msg.sender);
    require(userBalance > 0);
    // ensure sender has approved transfer
    uint256 amountToUpgrade = oldMovi.allowance(msg.sender, address(this));
    require(amountToUpgrade > 0 && amountToUpgrade <= userBalance);
    // transfer old tokens to this contract, then new to sender.
    oldMovi.transferFrom(msg.sender, address(this), amountToUpgrade);
    newMovi.transfer(msg.sender, amountToUpgrade);
    emit TokensUpgraded(msg.sender, amountToUpgrade);
    return amountToUpgrade;
  }
}
