// const MoviToken = artifacts.require("MoviToken");
// const MoviToken2 = artifacts.require("MoviToken2");
//const MoviTokenSwapManager = artifacts.require("MoviTokenSwapManager");
const NewMoviSwapManager = artifacts.require("NewMoviSwapManager");

// module.exports = function(deployer) {
//   deployer.deploy(MoviToken).then(function(instance){
//     console.log("instance of token: ", MoviToken.address);
//     return deployer.deploy(MoviTokenSwapManager, MoviToken.address);
//   }).then(function(instance){
//     console.log("instance of token: ", MoviTokenSwapManager.address);
//     return MoviTokenSwapManager.deployed();
//   });
// };

module.exports = function(deployer, network, accounts) {
  deployer.then(async () => {
    // movi = await deployer.deploy(MoviToken);
    // originalMoviAddress
    swap = await deployer.deploy(NewMoviSwapManager);
    swapDeployed = await NewMoviSwapManager.deployed();
    console.log("swap: ", swapDeployed);
    return swapDeployed;
  }).then(async (swap) => {
    const newMovi = await swap.getNewMovi();
    console.log('newMovi: ', newMovi);
    // return newMovi;
  });
}
