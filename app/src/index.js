import Web3 from 'web3';
import oldMoviArtifact from '../../build/contracts/MoviToken.json';
import newMoviArtifact from '../../build/contracts/MoviToken2.json';
import swapArtifact from '../../build/contracts/MoviTokenSwapManager.json';

const App = {
  networkId: null,
	deployments: {
		kovan: {
			oldMovi: "0xff9b33ADCD46409d01F76625A94737EA423531fD",
			newMovi: "0xDb282e700D6fb0352E65249F59Ae30C4b42Be050",
			swap: "0x40d0F7F933DE18A2d2b5dfb16Fd851E72Ada2082"
		},
		live: {
			oldMovi: "0x623b925b0a57a24ea8de301f2e3e692ce903f0c3",
      newMovi: "0x3BD4b0Ec2808F9100Cfef299cc73753e165f5a99",
      swap: "0xe0a9e3Abf908a22bFe47639C4a75d96233675Db3"
		}
	},
  web3: null,
  account: null,
  swap: null,
  swapAddress: null,
  oldMovi: null,
  newMovi: null,
  oldMoviAddress: null,
  newMoviAddress: null,

  start: async function() {
    const { web3, deployments: { live } } = this;
    try {
      const networkId = await web3.eth.net.getId();
      this.networkId = networkId;
      const deployedMovi = live.oldMovi;
      this.oldMoviAddress = deployedMovi;
      this.oldMovi = await new web3.eth.Contract(oldMoviArtifact.abi, deployedMovi);
      this.swapAddress = live.swap;
      this.swap = await new web3.eth.Contract(swapArtifact.abi, this.swapAddress);
      const newMoviAddress = live.newMovi; 
      this.newMoviAddress = newMoviAddress;
      this.newMovi = await new web3.eth.Contract(newMoviArtifact.abi, newMoviAddress);
      const accounts = await web3.eth.getAccounts();
      this.account = accounts[0];
      this.refreshBalance();
      this.refreshNewBalance();
      this.displayAddresses();
      this.setContractAddresses();

    } catch (error) {
      console.error('Could not connect to contract or chain.');
    }
    this.setSwapStatus();
    web3.currentProvider.publicConfigStore.on('update',(e) => {
      if(App.account == e.selectedAddress) { return; }
      App.account = e.selectedAddress;
      App.refreshNewBalance();
      App.refreshBalance();
    });
  },

  setContractAddresses: function() {
    const movi = this.oldMoviAddress;
    const movii = this.newMoviAddress;
    const moviDiv = document.getElementById('old-contract');
    const moviiDiv = document.getElementById('new-contract');
    const moviAnchor = this.addrAnchor({addr:movi});
    const moviiAnchor = this.addrAnchor({addr:movii});
    moviDiv.innerHTML = '';
    moviDiv.appendChild(moviAnchor);
    moviiDiv.innerHTML = '';
    moviiDiv.appendChild(moviiAnchor);
  },

  addrAnchor: function({addr, network}) {
    let n = "";
    if(network === "kovan"){
      n="kovan.";
    }
    const anch = document.createElement('a');
    const text = document.createTextNode(addr);
    anch.innerHTML = addr;
    anch.setAttribute('href', `https://${n}etherscan.io/address/${addr}`)
    return anch;
  },

  pending: function(status){
    if(status){
      document.getElementById('status-pending').className = "lds-ring"
      document.getElementById('approve-btn').setAttribute("disabled","disabled")
      document.getElementById('swap-btn').setAttribute("disabled","disabled")
    } else {
      document.getElementById('status-pending').className = "disabled";
      document.getElementById('approve-btn').removeAttribute("disabled")
      document.getElementById('swap-btn').removeAttribute("disabled");
    }
  },

  displayAddresses: function() {
    document.getElementById('old-contract').innerHTML = this.oldMoviAddress;
    document.getElementById('new-contract').innerHTML = this.newMoviAddress;
  },

  refreshNewBalance: async function() {
    const {balanceOf} = this.newMovi.methods;
    //this.pending(true);
    const balance = await balanceOf(this.account).call();
    //this.pending(false);
    const balanceElement = document.getElementById('new-balance');
    balanceElement.innerHTML = balance;
  },

  refreshBalance: async function() {
    const { balanceOf } = this.oldMovi.methods;
    // this.pending(true);
    const balance = await balanceOf(this.account).call();
    // this.pending(false);
    const balanceElement = document.getElementById('old-balance');
    if(balance <= 0){
      document.getElementById("approve-btn").setAttribute("disabled", "disabled");
    }
    balanceElement.innerHTML = balance;
  },

  approveMoviTransfer: async function() {
    const { approve } = this.oldMovi.methods;
    //this.pending(true);
    this.setStatus("getting balance...");
    const { balanceOf } = this.oldMovi.methods;
    const balance = await balanceOf(this.account).call();
    this.setStatus("requesting your permission to transfer your old MOVI...");
    if(balance <= 0){return;}
    const approval = await approve(this.swapAddress,balance).send({from: this.account});
    if(approval){
      this.setStatus(`upgrade approved.`);
    } else {
      this.setStatus('upgrade not approved. Check that your correct address is selected and you have enough balance.');
    }
    this.setSwapStatus()
    // this.pending(false);
  },

  setSwapStatus: async function() {
    const { allowance } = this.oldMovi.methods;
    // this.pending(true);
    const swapButton = document.getElementById("swap-btn");
    const amt = await allowance(this.account, this.swapAddress).call();
    console.log("swap status", amt)
    // this.pending(false);
    if(amt > 0){
      swapButton.removeAttribute("disabled");
      document.getElementById('allowance').innerHTML = "upgrade approved"
    } else {
      swapButton.setAttribute("disabled", "disabled");
      document.getElementById('allowance').innerHTML = "no MOVI to upgrade"
    }
  },

  withdrawTokens: async function() {
    const { withdrawTokens } = this.swap.methods;
    this.pending(true);
    try{
      this.setStatus("swapping tokens...");
      const result = await withdrawTokens().send({value:0, gas: 300000, from: this.account});
      if(result.events["TokensUpgraded"]){
        this.setStatus("upgrade complete!");
      }
    } catch (e) {
      // this.pending(false);
      this.setStatus("token upgrade failed.")
    }
    this.pending(false);
    this.refreshBalance();
    this.refreshNewBalance();
    this.setSwapStatus();
  },

  setStatus: function(message) {
    const status = document.getElementById('status');
    status.innerHTML = message;
  },
};

window.App = App;

window.addEventListener('load', function() {
  if (window.ethereum) {
    // use MetaMask's provider
    App.web3 = new Web3(window.ethereum);
    window.ethereum.enable();
  } else {
    const swapAppEl = document.getElementById('swap-app');

    const classes = swapAppEl.className;
    swapAppEl.className = classes.concat(' disabled');
    
    console.warn(
      'No web3 detected. Falling back to http://127.0.0.1:9545. You should remove this fallback when you deploy live',
    );
    // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
    App.web3 = new Web3(
      new Web3.providers.HttpProvider('http://127.0.0.1:9545'),
    );
  }

  App.start();
});
